const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const rimraf = require('rimraf');
const program = require('commander');
const { version } = require('./package.json');

let src, dest;

/**
 * Copy to the folder corresponding to the first letter of the file name
 * @param {String} file - sorce file
 * @param {Function} cb
 */
function copy (file, cb) {
  const fileName = path.basename(file);
  const destDir = path.join(dest, fileName[0].toUpperCase());
  const destFile = path.join(destDir, fileName);

  mkdirp(destDir, err => {
    if (err) return cb(err);

    fs.copyFile(file, destFile, err => cb(err));
  });
}

/**
 * Walking by folders
 * @param {String} src - source dir
 * @param {Function} cb
 */
const walk = (src, cb) => fs.readdir(src, (err, list) => {
  if (err) return cb(err);

  let pending = list.length;

  if (!pending) return cb();

  list.forEach(file => {
    file = path.resolve(src, file);
    fs.stat(file, (err, status) => {
      if (err) return cb(err);

      if (status.isDirectory()) {
        walk(file, () => {
          if (!--pending) cb();
        });
      } else {
        copy(file, () => {
          if (!--pending) cb();
        });
      }
    });
  });
});

program
  .version(version)
  .usage('<src_dir> <dest_dir> [options]')
  .option('-D, --delete', 'remove source folder')
  .parse(process.argv);

if (program.args.length < 2) {
  console.log(program.args);
  console.error('Invalid number of arguments!');
  process.exit(-1);
}

[src, dest] = program.args;

walk(src, (err) => {
  if (err) throw err;

  console.log('All files is copied');

  if (program.delete) {
    rimraf(src, err => {
      if (err) throw err;

      console.log('Source directory is deleted');
    });
  }
});
